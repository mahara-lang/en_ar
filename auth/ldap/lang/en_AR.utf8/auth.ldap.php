<?php
/* Pirate language file generated Fri May  2 16:19:31 2008 */

$string['title'] = 'LDAP';
$string['description'] = 'Authenticate against an LDAP server';
$string['notusable'] = 'Please install th\' PHP LDAP extension';
$string['contexts'] = 'Contexts';
$string['distinguishedname'] = 'Distinguished name';
$string['hosturl'] = 'Host URL';
$string['ldapfieldforemail'] = 'LDAP field fer Email';
$string['ldapfieldforfirstname'] = 'LDAP field fer First Name';
$string['ldapfieldforsurname'] = 'LDAP field fer Surname';
$string['ldapversion'] = 'LDAP version';
$string['password'] = 'Password';
$string['searchsubcontexts'] = 'Search subcontexts';
$string['userattribute'] = 'User attribute';
$string['usertype'] = 'User type';
$string['weautocreateusers'] = 'We auto-create users';

?>
