<?php
/* Pirate language file generated Fri May  2 16:18:56 2008 */

$string['internal'] = 'Internal';
$string['title'] = 'Internal';
$string['description'] = 'Authenticate against Mahara`s database';
$string['completeregistration'] = 'Complete Registration';
$string['emailalreadytaken'] = 'This e-mail address has already registered here';
$string['iagreetothetermsandconditions'] = 'I agree t\' th\' Terms an\' Conditions';
$string['passwordformdescription'] = 'Your password must be at least six characters long an\' contain at least one digit an\' two letters';
$string['passwordinvalidform'] = 'Your password must be at least six characters long an\' contain at least one digit an\' two letters';
$string['registeredemailsubject'] = 'You be havin\' registered at %s';
$string['registeredok'] = '<p>Ye be havin\' successfully registered. Please check yer e-mail account fer instructions on how t\' activate yer account</p>';
$string['registrationnosuchkey'] = 'Sorry, thar dasn\'t seem t\' be a registration wi\' this key. Perhaps ye waited longer than 24 hours t\' complete yer registration? Otherwise, \'t might be our fault.';
$string['registrationunsuccessful'] = 'Sorry, yer registration attempt be unsuccessful. This be our fault, nay yours. Please try again later.';
$string['usernamealreadytaken'] = 'Sorry, this username be already taken';
$string['usernameinvalidform'] = 'Your username may only include alphanumeric characters, full stops, underscores an\' @ symbols. Also, \'t must be between 3 an\' 30 characters long.';
$string['youmaynotregisterwithouttandc'] = 'You may nay register unless ye agree t\' abide by th\' <a href="terms.php">Terms an\' Conditions</a>';
$string['youmustagreetothetermsandconditions'] = 'You must agree t\' th\' <a href="terms.php">Terms an\' Conditions</a>';

?>
