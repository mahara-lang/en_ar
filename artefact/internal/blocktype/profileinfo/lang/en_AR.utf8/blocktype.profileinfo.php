<?php
/* Pirate language file generated Fri May  2 16:17:05 2008 */

$string['title'] = 'Profile Information';
$string['description'] = 'Choose profile information t\' display';
$string['aboutme'] = 'About Me';
$string['fieldstoshow'] = 'Fields t\' show';
$string['introtext'] = 'Introduction Text';
$string['useintroductioninstead'] = 'You can use yer introduction profile field instead by enablin\' that an\' leavin\' this field blank';
$string['dontshowprofileicon'] = 'Don`t show a profile icon';
$string['dontshowemail'] = 'Don`t show email address';

?>
