<?php
/* Pirate language file generated Fri May  2 16:14:26 2008 */

$string['title'] = 'An Image';
$string['description'] = 'A single image from yer Files area';
$string['showdescription'] = 'Show Description?';
$string['width'] = 'Width';
$string['widthdescription'] = 'Specify th\' width fer yer image (in pixels). Th\' image be scaled t\' this width. Leave \'t blank t\' use th\' original size o\' th\' image.';

?>
