<?php
/* Pirate language file generated Fri May  2 16:17:17 2008 */

$string['pluginname'] = 'Resumé';
$string['myresume'] = 'My Resumé';
$string['mygoals'] = 'My Goals';
$string['myskills'] = 'My Skills';
$string['coverletter'] = 'Co\'er Letter';
$string['interest'] = 'Interests';
$string['contactinformation'] = 'Contact Information';
$string['personalinformation'] = 'Swabbieal Information';
$string['dateofbirth'] = 'Date o\' birth';
$string['placeofbirth'] = 'Place o\' birth';
$string['citizenship'] = 'Citizenship';
$string['visastatus'] = 'Visa Status';
$string['female'] = 'Female';
$string['male'] = 'Male';
$string['gender'] = 'Gender';
$string['maritalstatus'] = 'Marital Status';
$string['resumesaved'] = 'Resumé saved';
$string['resumesavefailed'] = 'Failed t\' update yer resumé';
$string['educationhistory'] = 'Education History';
$string['employmenthistory'] = 'Employment History';
$string['certification'] = 'Certifications, Accreditations, an\' Awards';
$string['book'] = 'Books an\' Publications';
$string['membership'] = 'Professional Memberships';
$string['startdate'] = 'Start date';
$string['enddate'] = 'End date';
$string['date'] = 'Date';
$string['position'] = 'Position';
$string['qualification'] = 'Qualification';
$string['description'] = 'Description';
$string['title'] = 'Title';
$string['employer'] = 'Employer';
$string['jobtitle'] = 'Job Title';
$string['jobdescription'] = 'Position Description';
$string['institution'] = 'Institution';
$string['qualtype'] = 'Qualification Type';
$string['qualname'] = 'Qualification Name';
$string['qualdescription'] = 'Qualification Description';
$string['contribution'] = 'Contribution';
$string['compositedeleteconfirm'] = 'Are ye sure ye want t\' delete this?';
$string['compositesaved'] = 'Saved successfully';
$string['compositesavefailed'] = 'Save failed';
$string['compositedeleted'] = 'Deleted successfully';
$string['backtoresume'] = 'Aft t\' Me Resumé';
$string['personalgoal'] = 'Swabbieal Goals';
$string['academicgoal'] = 'Academic Goals';
$string['careergoal'] = 'Career Goals';
$string['personalskill'] = 'Swabbieal Skills';
$string['academicskill'] = 'Academic Skills';
$string['workskill'] = 'Work Skills';
$string['goalandskillsaved'] = 'Saved successfully';
$string['resume'] = 'Resumé';
$string['current'] = 'Current';
$string['moveup'] = 'Move Up';
$string['movedown'] = 'Move Down';

?>
