<?php
/* Pirate language file generated Fri May  2 16:59:51 2008 */

$string['title'] = 'External Feed';
$string['description'] = 'Embed an external RSS or ATOM feed';
$string['feedlocation'] = 'Feed location';
$string['feedlocationdesc'] = 'URL o\' a valid RSS or ATOM feed';
$string['showfeeditemsinfull'] = 'Show feed items in full?';
$string['showfeeditemsinfulldesc'] = 'Whether t\' show a summary o\' th\' feed items, or show th\' description fer each one too';
$string['invalidurl'] = 'That URL be invalid. Ye can only view feeds fer http an\' https URLs.';
$string['invalidfeed'] = 'The feed appears t\' be invalid. Th\' error reported be: %s';
$string['lastupdatedon'] = 'Last updated on %s';
$string['defaulttitledescription'] = 'If ye leave this blank, th\' title o\' th\' feed be used';

?>
