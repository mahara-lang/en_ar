<?php
/* Pirate language file generated Fri May  2 16:20:00 2008 */

$string['changepassworddesc'] = 'If ye wish t\' change yer password, please enter th\' details here';
$string['changepasswordotherinterface'] = 'You may  <a href="%s">change yer password</a> through a different interface</a>';
$string['oldpasswordincorrect'] = 'This be nay yer current password';
$string['accountoptionsdesc'] = 'You can set general account options here';
$string['friendsnobody'] = 'Nobody may add me as a friend';
$string['friendsauth'] = 'New shipmates require me authorisation';
$string['friendsauto'] = 'New shipmates be automatically authorised';
$string['friendsdescr'] = 'Friends control';
$string['updatedfriendcontrolsetting'] = 'Updated shipmates control';
$string['wysiwygdescr'] = 'HTML editor';
$string['on'] = 'On';
$string['off'] = 'Off';
$string['messagesdescr'] = 'Messages from other users';
$string['messagesnobody'] = 'Do nay allow ere t\' send me messages';
$string['messagesfriends'] = 'Allow swabbies on me Shipmates list t\' send me messages';
$string['messagesallow'] = 'Allow ere t\' send me messages';
$string['language'] = 'Language';
$string['showviewcolumns'] = 'Show controls t\' add an\' remove columns when editin\' a view';
$string['prefssaved'] = 'Preferences saved';
$string['prefsnotsaved'] = 'Failed t\' save yer Preferences!';

?>
