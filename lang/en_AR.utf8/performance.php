<?php
/* Pirate language file generated Fri May  2 16:52:19 2008 */

$string['memoryused'] = 'Memory';
$string['timeused'] = 'Execution time';
$string['seconds'] = 'seconds';
$string['included'] = 'Included files';
$string['dbqueries'] = 'DB queries';
$string['reads'] = 'reads';
$string['writes'] = 'writes';
$string['ticks'] = 'ticks';
$string['sys'] = 'sys';
$string['user'] = 'user';
$string['cuser'] = 'cuser';
$string['csys'] = 'csys';
$string['serverload'] = 'Ser\'er load';

?>
