<?php
/* Pirate language file generated Fri May  2 16:37:27 2008 */

$string['parentlanguage'] = 'Stop wastin\' me time an\' put somethin\' in th\' box!';
$string['strftimedate'] = '%%d %%B %%Y';
$string['strftimedateshort'] = '%%d %%B';
$string['strftimedatetime'] = '%%d %%B %%Y, %%l:%%M %%p';
$string['strftimedaydate'] = '%%A, %%d %%B %%Y';
$string['strftimedaydatetime'] = '%%A, %%d %%B %%Y, %%l:%%M %%p';
$string['strftimedayshort'] = '%%A, %%d %%B';
$string['strftimedaytime'] = '%%a, %%k:%%M';
$string['strftimemonthyear'] = '%%B %%Y';
$string['strftimerecent'] = '%%d %%b, %%k:%%M';
$string['strftimerecentfull'] = '%%a, %%d %%b %%Y, %%l:%%M %%p';
$string['strftimetime'] = '%%l:%%M %%p';
$string['strfdaymonthyearshort'] = '%%d/%%m/%%Y';
$string['thislanguage'] = 'English';

?>
