<?php
/* Pirate language file generated Fri May  2 16:30:17 2008 */

$string['host'] = 'Hostname or address';
$string['wwwroot'] = 'WWW root';
$string['protocol'] = 'Protocol';
$string['changepasswordurl'] = 'Password-change URL';
$string['cannotremove'] = 'We canna remove this auth plugin, as \'tis th\' only \nplugin that exists fer this institution.';
$string['cannotremoveinuse'] = 'We canna remove this auth plugin, as \'tis bein\' used by some users.\nYou must update the\'r records before ye can remove this plugin.';
$string['saveinstitutiondetailsfirst'] = 'Please save th\' institution details before configurin\' authentication plugins.';
$string['editauthority'] = 'Edit an Authority';
$string['addauthority'] = 'Add an Authority';
$string['updateuserinfoonlogin'] = 'Update user info on login';
$string['updateuserinfoonlogindescription'] = 'Retrieve user info from th\' remote ser\'er an\' update yer local user record each time th\' user logs in.';
$string['xmlrpcserverurl'] = 'XML-RPC Ser\'er URL';
$string['ipaddress'] = 'IP Address';
$string['shortname'] = 'Short name fer yer site';
$string['name'] = 'Site name';
$string['nodataforinstance'] = 'Couldna find data fer auth instance';
$string['authname'] = 'Authority name';
$string['weautocreateusers'] = 'We auto-create users';
$string['theyautocreateusers'] = 'They auto-create users';
$string['parent'] = 'Parent authority';
$string['wessoout'] = 'We SSO out';
$string['theyssoin'] = 'They SSO in';
$string['application'] = 'Application';
$string['cantretrievekey'] = 'An error occurred while retrievin\' th\' public key from th\' remote server.<br>Please ensure that th\' Application an\' WWW Root fields be correct, an\' that networkin\' be enabled on th\' remote host.';
$string['errorcertificateinvalidwwwroot'] = 'This certificate claims t\' be fer %s, but ye be tryin\' t\' use \'t fer %s.';
$string['errnoauthinstances'] = 'We don`t seem t\' be havin\' any authentication plugin instances configured fer th\' host at';
$string['errornotvalidsslcertificate'] = 'This be nay a valid SSL Certificate';
$string['errnoxmlrcpinstances'] = 'We don`t seem t\' be havin\' any XMLRPC authentication plugin instances configured fer th\' host at';
$string['errnoxmlrcpwwwroot'] = 'We don`t be havin\' a record fer any host at';
$string['unabletosigninviasso'] = 'Unable t\' sign in via SSO';
$string['xmlrpccouldnotlogyouin'] = 'Sorry, couldna log ye in :(';
$string['xmlrpccouldnotlogyouindetail'] = 'Sorry, we couldna log ye into Mahara at this time. Please tryin again shortly, an\' if th\' problem persists, contact yer administrator';
$string['requiredfields'] = 'Required profile fields';
$string['requiredfieldsset'] = 'Required profile fields set';

?>
