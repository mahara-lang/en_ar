<?php
/* Pirate language file generated Fri May  2 16:31:12 2008 */

$string['phpversion'] = 'Mahara will nay run on PHP < %s. Please upgrade yer PHP version, or move Mahara t\' a different host.';
$string['jsonextensionnotloaded'] = 'Your ser\'er configuration dasn\'t include th\' JSON extension. Mahara requires this in order t\' send some data t\' an\' from th\' browser. Please make sure that \'tis loaded in php.ini, or install \'t if \'tis nay installed.';
$string['pgsqldbextensionnotloaded'] = 'Your ser\'er configuration dasn\'t include th\' pgsql extension. Mahara requires this in order t\' store data in a relational database. Please make sure that \'tis loaded in php.ini, or install \'t if \'tis nay installed.';
$string['mysqldbextensionnotloaded'] = 'Your ser\'er configuration dasn\'t include th\' mysql extension. Mahara requires this in order t\' store data in a relational database. Please make sure that \'tis loaded in php.ini, or install \'t if \'tis nay installed.';
$string['mysqldbtypedeprecated'] = 'You be usin\' th\' dbtype "mysql" in yer config file. Please change \'t t\' "mysql5" - "mysql" be deprecated.';
$string['unknowndbtype'] = 'Your ser\'er configuration references an unknown database type. Valid values be "postgres8" an\' "mysql5". Please change th\' database type settin\' in config.php.';
$string['libxmlextensionnotloaded'] = 'Your ser\'er configuration dasn\'t include th\' libxml extension. Mahara requires this in order t\' parse XML data fer th\' installer an\' fer aftups. Please make sure that \'tis loaded in php.ini, or install \'t if \'tis nay installed.';
$string['gdextensionnotloaded'] = 'Your ser\'er configuration dasn\'t include th\' gd extension. Mahara requires this in order t\' perform resizes an\' other operations on uploaded images. Please make sure that \'tis loaded in php.ini, or install \'t if \'tis nay installed.';
$string['sessionextensionnotloaded'] = 'Your ser\'er configuration dasn\'t include th\' session extension. Mahara requires this in order t\' support users loggin\' in. Please make sure that \'tis loaded in php.ini, or install \'t if \'tis nay installed.';
$string['registerglobals'] = 'You be havin\' dangerous PHP settings, register_globals be on. Mahara be tryin\' t\' work around this, but ye ortin\' ta really fix it';
$string['magicquotesgpc'] = 'You be havin\' dangerous PHP settings, magic_quotes_gpc be on. Mahara be tryin\' t\' work around this, but ye ortin\' ta really fix it';
$string['magicquotesruntime'] = 'You be havin\' dangerous PHP settings, magic_quotes_runtime be on. Mahara be tryin\' t\' work around this, but ye ortin\' ta really fix it';
$string['magicquotessybase'] = 'You be havin\' dangerous PHP settings, magic_quotes_sybase be on. Mahara be tryin\' t\' work around this, but ye ortin\' ta really fix it';
$string['datarootinsidedocroot'] = 'You be havin\' set up yer data root t\' be inside yer document root. This be a large security problem, as then ere can directly request session data (in order t\' hijack other swabbiess` sessions), or files that they be nay allowed t\' access that other swabbies be havin\' uploaded. Please configure th\' data root t\' be abroadside o\' th\' document root.';
$string['datarootnotwritable'] = 'Your defined data root directory, %s, be nay writable. This means that neither session data, user files nor anythin\' else that needs t\' be uploaded can be saved on yer server. Please make th\' directory if \'t dasn\'t exist, or give ownership o\' th\' directory t\' th\' web ser\'er user if \'t does.';
$string['couldnotmakedatadirectories'] = 'For some reason some o\' th\' core data directories couldna be created. This ortin\'t happen, as Mahara previously detected that th\' dataroot directory be writable. Please check th\' permissions on th\' dataroot directory.';
$string['blocktypenametaken'] = 'Block type %s be already taken by another plugin (%s)';
$string['artefacttypenametaken'] = 'Artefact type %s be already taken by another plugin (%s)';
$string['classmissing'] = 'class %s fer type %s in plugin %s be missing';
$string['artefacttypeclassmissing'] = 'Artefact types must all implement a class.  Missin\' %s';
$string['artefactpluginmethodmissing'] = 'Artefact plugin %s must implement %s an\' doesn`t';
$string['blocktypelibmissing'] = 'Missin\' lib.php fer block %s in artefact plugin %s';
$string['blocktypemissingconfigform'] = 'Block type %s must implement instance_config_form';
$string['versionphpmissing'] = 'Plugin %s %s be missin\' version.php!';
$string['blocktypeprovidedbyartefactnotinstallable'] = 'This be installed as part o\' th\' installation o\' artefact plugin %s';
$string['blockconfigdatacalledfromset'] = 'Configdata ortin\'t be set directly, use PluginBlocktype::instance_config_save instead';
$string['invaliddirection'] = 'Invalid direction %s';
$string['unrecoverableerror'] = 'A nonrecoverable error occurred.  This probably means that ye be havin\' encountered a bug in th\' system.';
$string['unrecoverableerrortitle'] = '%s - Site Unavailable';
$string['parameterexception'] = 'A required parameter be missing';
$string['accessdeniedexception'] = 'You do nay be havin\' access t\' view this page';
$string['notfound'] = 'Not Found';
$string['notfoundexception'] = 'The page ye be lookin\' fer couldna be found';
$string['accessdenied'] = 'Access Denied';
$string['accessdeniedexception'] = 'You do nay be havin\' access t\' view this page';
$string['viewnotfoundexceptiontitle'] = 'View nay found';
$string['viewnotfoundexceptionmessage'] = 'You tried t\' access a view that didn`t exist!';
$string['viewnotfound'] = 'View wi\' id %s nay found';
$string['artefactnotfoundmaybedeleted'] = 'Artefact wi\' id %s nay found (maybe \'t be deleted already?)';
$string['artefactnotfound'] = 'Artefact wi\' id %s nay found';
$string['notartefactowner'] = 'You do nay own this artefact';
$string['blockinstancednotfound'] = 'Block instance wi\' id %s nay found';
$string['interactioninstancenotfound'] = 'Activity instance wi\' id %s nay found';
$string['invalidviewaction'] = 'Invalid view control action: %s';
$string['missingparamblocktype'] = 'Try selectin\' a block type t\' add first';
$string['missingparamcolumn'] = 'Missin\' column specification';
$string['missingparamorder'] = 'Missin\' order specification';
$string['missingparamid'] = 'Missin\' id';

?>
